package com.thomsonreuters.grc.accelus.regint.bdd.pageobjects;

import com.thomsonreuters.grc.accelus.regint.bdd.support.Driver;
import com.thomsonreuters.grc.accelus.regint.bdd.selectors.SearchSelectors;

public class Search extends Driver {

    public static void navigateToSearchPage(){
        driver.get(SearchSelectors.GOOGLE_HOME_PAGE);
    }

}
