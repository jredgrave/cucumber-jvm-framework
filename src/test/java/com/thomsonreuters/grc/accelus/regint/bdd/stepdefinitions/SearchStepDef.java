package com.thomsonreuters.grc.accelus.regint.bdd.stepdefinitions;

import com.thomsonreuters.grc.accelus.regint.bdd.pageobjects.*;
import cucumber.api.java.en.*;


public class SearchStepDef {

    @Given("^I am on the google search page$")
    public void navigateToSearch() throws Throwable {
        Search.navigateToSearchPage();
    }

    @When("^I search for the term \"([^\"]*)\"$")
    public void I_search_for_the_term(String search_term) throws Throwable {
        // Express the Regexp above with the code you wish you had
        //throw new PendingException();
    }

    @Then("^the Thomson Reuters company website link is returned within the results$")
    public void the_Thomson_Reuters_company_website_link_is_returned_within_the_results() throws Throwable {
        // Express the Regexp above with the code you wish you had
        //throw new PendingException();
    }

}
